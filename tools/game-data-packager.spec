# Copyright 2015-2016 Alexandre Detiste
# Copyright 2016-2018 Simon McVittie
# SPDX-License-Identifier: GPL-2.0-only

# Fedora/RPMFusion version now at:
# https://pkgs.rpmfusion.org/cgit/free/game-data-packager.git/tree/game-data-packager.spec

# SuSE version now at:
# https://build.opensuse.org/package/view_file/games:tools/game-data-packager/game-data-packager.spec

#define gitdate 20160112
# git log --oneline -1
%define gitversion 50f64b6

%if 0%{?gitdate}
%define gver .git%{gitdate}%{gitversion}
%endif

Name:          game-data-packager
Version:       72
Release:       0%{?gver}%{?dist}
Summary:       Installer for game data files
License:       GPLv2 and GPLv2+
URL:           https://wiki.debian.org/Games/GameDataPackager
%if 0%{?gitdate}
# git archive --prefix=game-data-packager-44/ --format tar.gz master > ../rpmbuild/SOURCES/game-data-packager-`date +%Y%m%d`.tar.gz
Source:        game-data-packager-%{gitdate}.tar.gz
%else
Source:        http://http.debian.net/debian/pool/contrib/g/game-data-packager/game-data-packager_%{version}.tar.xz
%endif
BuildArch:     noarch
BuildRequires: meson
BuildRequires: desktop-file-utils
BuildRequires: ImageMagick
BuildRequires: inkscape
BuildRequires: python3
BuildRequires: python3-pyyaml
BuildRequires: python3-pyflakes
BuildRequires: xmlstarlet
BuildRequires: zip
Requires: python3-pyyaml
# download
Recommends: lgogdownloader
Suggests: steam
# rip
Suggests: cdparanoia
Suggests: vorbis-tools
# extract
Suggests: arj
Suggests: cabextract
Recommends: innoextract
Suggests: lha
Suggests: p7zip-plugins
Suggests: xdelta
Suggests: xdelta3
Suggests: unar
Suggests: unrar
Suggests: unshield
Suggests: unzip
Suggests: xorriso
# cross-build for RaspBian
Suggests: dpkg
Suggests: python3-debian

%global __python %{__python3}

%description
Various games are divided into two logical parts: engine and data.
.
game-data-packager is a tool which builds .rpm files for game
data which cannot be distributed (such as commercial game data).

%package -n runtime
Summary: Launchers for games supported by game-data-packager
Requires: python3-gobject-base
Requires: gobject-introspection
Requires: gtk4
Obsoletes: game-data-packager-doom2-masterlevels
%description -n runtime
Various games are divided into two logical parts: engine
and data. Often the engine and data are licensed in
different ways, such that the engine can be distributed
in free operating systems but the data cannot.
.
Some of the games supported by game-data-packager need to be
started by a special launcher script that will create symbolic
links or do other setup. This package contains those launcher
scripts.
.
The following games currently require this package:
.
 * Doom 2: The Master Levels
 * Unreal
 * Unreal Gold

%prep
%autosetup

%build
%meson
%meson_build

%check
make check

%install
%meson_install
find %{buildroot}%{_datadir}/game-data-packager/game_data_packager -name '*.py' -exec chmod 755 {} \;
#E: python-bytecode-inconsistent-mtime
python3 -m compileall %{buildroot}%{_datadir}/game-data-packager/game_data_packager/version.py
find %{buildroot}%{_sysconfdir}/game-data-packager -empty -exec sh -c "echo '# we need more mirrors' > {}" \;
rm -rvf %{buildroot}%{_sysconfdir}/apparmor.d

%files
%doc doc/adding_a_game.mdwn
%{_mandir}/man6/game-data-packager.*
%{_mandir}/fr/man6/game-data-packager.*
%dir %{_sysconfdir}/game-data-packager/
%config(noreplace) %{_sysconfdir}/game-data-packager.conf
%config(noreplace) %{_sysconfdir}/game-data-packager/*
%{_bindir}/game-data-packager
%{_datadir}/bash-completion/completions/game-data-packager
%{_datadir}/game-data-packager
%license COPYING

%files -n runtime
%{_mandir}/man6/doom2-masterlevels.*
%{_bindir}/doom2-masterlevels
%{_datadir}/applications/net.debian.game_data_packager.doom2_masterlevels.desktop
%{_datadir}/pixmaps/doom2-masterlevels.png
/usr/share/game-data-packager-runtime
%license COPYING

%changelog
* Sat Jul 23 2016 Alexandre Detiste <alexandre.detiste@gmail.com> - 45-1
- Initial port to Mageia, skip one icon that makes ImageMagick choke
