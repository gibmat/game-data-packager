#!/usr/bin/python3
#
# Copyright © 2023 Alexandre Detiste <alexandre@detiste.be>
# SPDX-License-Identifier: GPL-2.0-or-later

# match engines known to GDP versus all engines in contrib
# to check if we missed any -> we did missed openrct2

# TODO:
# - also read ITP/RFP from wnpp

import apt

from game_data_packager.game import (load_games)
from game_data_packager.packaging.deb import (DebPackaging)
packaging = DebPackaging()


def get_engines() -> set[str]:
    '''all engines known to GDP'''

    engines: set[str] = set()

    for name, game in load_games().items():
        game.load_file_data()
        if game.binary_executables:
            # GDP also ship the binary engine
            continue
        for package in game.packages.values():
            if package.section in ('fonts',):
                continue
            engine = package.engine or game.engine
            assert engine, name
            if type(engine) is dict:
                engine = packaging.substitute(engine, name)
            assert type(engine) is str
            engine = engine.split(' ')[0]
            engines.add(engine)
    return engines


engines = get_engines()


def get_tools() -> set[str]:
    '''Recommended & Suggests tools that may be in contrib'''
    tools: set[str] = set()
    return tools


cache = apt.Cache()

all: dict[str, str] = dict()
main: dict[str, str] = dict()
contrib: dict[str, str] = dict()
non_free: dict[str, str] = dict()

for k in cache.keys():
    p = cache[k]
    # print(dir(p))
    if p.candidate and p.candidate.downloadable:
        c = p.candidate
        # print(dir(c))
        if '/' in c.section:
            archive, section = c.section.split('/')
        else:
            archive = 'main'
            section = c.section
        all[p.shortname] = section
        if archive == 'contrib':
            contrib[p.shortname] = section
        elif archive == 'non-free':
            non_free[p.shortname] = section
        else:
            main[p.shortname] = section

for engine in sorted(engines):
    if engine not in all:
        print('%s is not packaged' % engine)
    elif all[engine] != 'games':
        print('%s is not a game: %s' % (engine, all[engine]))

for maybe_engine, section in sorted(contrib.items()):
    if maybe_engine in engines:
        pass
    elif section == 'games':
        print(maybe_engine)

for maybe_engine, section in sorted(non_free.items()):
    if maybe_engine in engines:
        pass
    elif section == 'games':
        print(maybe_engine)
